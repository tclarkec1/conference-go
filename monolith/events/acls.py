import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Use the Pexels API
    url = f"https://api.pexels.com/v1/search?query={city}&{state}/"
    headers = {"AUTHORIZATION": PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    photos = json.loads(r.content)

    # create dict
    photo = {
        "picture_url": photos["photos"][0]["src"]["original"]

    }
    return photo
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_weather_data(city, state):
    parameters = {"q": f"{city}, {state}, US"}
    geo_url = "https://api.openweathermap.org/geo/1.0/direct/"
    headers = {"AUTHORIZATION": OPEN_WEATHER_API_KEY}
    response = requests.get(geo_url, headers=headers, parameters=parameters)
    content = response.json(response.content)
    latitude = content[0]["lat"]
    longitude = content[0]["lon"]

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
    }
    weather_url = "https://api.openweathermap.org/data/2.5/weather/"

    weather_response = requests.get(weather_url, params=params)
    content = response.json(weather_response.content)
    weather_dict = {
        "description": content["weather"][0]["description"],
        "temperature": content["main"]["temp"]
        }

    return weather_dict

    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
